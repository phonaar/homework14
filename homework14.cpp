#include <iostream>
#include <string>

int main()
{
    std::cout << "Enter your Value: ";
    std::string variable;
    std::getline(std::cin, variable);
   
    
    std::cout << "Value = " << variable << "\n";
    std::cout << "Length = " << variable.length() << "\n";
    std::cout << "First symbol is - " << variable.front() << "\n";
    std::cout << "Last symbol is - " << variable.back() << "\n";
    return 0;
}
